package nl.benkhard.springbootmavenexample.service;

import nl.benkhard.springbootmavenexample.exception.NameAlreadyExistsException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ExampleService {
    private List<String> names;

    public ExampleService() {
        this.names = new ArrayList<>();
    }

    /**
     * Get a list of all the names
     * @return A list of names as Strings
     */
    public List<String> getNames() {
        return names;
    }

    /**
     * Add a new name to the collection. Name should be unique
     * or exception will be thrown.
     * @param name The name that should be stored
     * @throws NameAlreadyExistsException
     */
    public void addName(String name) throws NameAlreadyExistsException {
        if(names.contains(name)) throw new NameAlreadyExistsException(name);
        names.add(name);
    }
}
