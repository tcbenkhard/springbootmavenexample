package nl.benkhard.springbootmavenexample.controller;

import nl.benkhard.springbootmavenexample.service.ExampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ExampleController {
    @Autowired
    ExampleService service;

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("names", service.getNames());
        return "home";
    }

    @PostMapping("/")
    public ModelAndView create(@RequestParam("name") String name, RedirectAttributes attributes) {
        ModelAndView view = new ModelAndView();
        view.setViewName("redirect:/");

        try {
            service.addName(name);
        } catch (Exception e) {
            attributes.addFlashAttribute("error", e.getMessage());
        }

        return view;
    }
}
