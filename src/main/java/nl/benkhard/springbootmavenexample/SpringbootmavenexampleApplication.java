package nl.benkhard.springbootmavenexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
public class SpringbootmavenexampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootmavenexampleApplication.class, args);
    }
}
