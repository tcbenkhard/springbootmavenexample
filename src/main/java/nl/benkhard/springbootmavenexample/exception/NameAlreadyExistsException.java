package nl.benkhard.springbootmavenexample.exception;

public class NameAlreadyExistsException extends Exception {
    private final static String MESSAGE = "The name '%s' was already entered.";
    private String name;

    public NameAlreadyExistsException(String name) {
        super(String.format(MESSAGE, name));
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
