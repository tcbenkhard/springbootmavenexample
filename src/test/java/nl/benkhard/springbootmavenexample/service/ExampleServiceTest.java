package nl.benkhard.springbootmavenexample.service;

import nl.benkhard.springbootmavenexample.exception.NameAlreadyExistsException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.annotation.DirtiesContext;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ExampleServiceTest {
    ExampleService service;

    @Before
    public void setUp() throws Exception {
        service = new ExampleService();
    }

    @Test
    public void getNamesNotNull() {
        List<String> names = service.getNames();
        assertNotNull(names);
    }

    @Test
    public void getNamesSizeZero() {
        List<String> names = service.getNames();
        assertEquals(0, names.size());
    }

    @Test
    @DirtiesContext
    public void addNewName() throws Exception {
        service.addName("Timo");
        service.addName("Manon");
        List<String> names = service.getNames();

        assertEquals(2, names.size());
    }

    @Test(expected = NameAlreadyExistsException.class)
    public void addExistingName() throws Exception {
        service.addName("Timo");
        service.addName("Timo");
    }
}